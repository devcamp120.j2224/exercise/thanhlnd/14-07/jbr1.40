public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        InvoiceItem invoiceItem1 = new InvoiceItem();
        InvoiceItem invoiceItem2 = new InvoiceItem("tcl", "bcs", 10, 10000);
        //thông tin hóa đơn
        System.out.println("thông tin hóa đơn " + invoiceItem1);
        System.out.println("thông tin hóa đơn " + invoiceItem2);
        //số tiền phải thanh toán
        System.out.println("số tiền phải thanh toán là " + invoiceItem1.getTotal());
        System.out.println("số tiền phải thanh toán là " + invoiceItem2.getTotal());
    }
}
